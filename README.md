

![GPL3](./gpl3.png "GPL3")

These software are covered by the GNU General Public License (GPL) version 3 and above
with the exception of some cataloged as "OUTDATED" which may be covered by a license the
GNU General Public License (GPL) version 2.

# LIST OF C CODE EXAMPLES REGISTERED HERE

| Example | Description |
| :------ | :---------- |
| **[example_of_checking_if_a_command_is_runnable](https://gitlab.com/mubunt/example_of_checking_if_a_command_is_runnable)** | functional example of how to check if a command/application is runnable before using it (with "system" by example). |
| **[example_of_customizing_terminal_size](https://gitlab.com/mubunt/example_of_customizing_terminal_size)** | functional example of customizing the terminal size  (column number) |
| **[example_of_finding_the_caller_of_my_caller](https://gitlab.com/mubunt/example_of_finding_the_caller_of_my_caller)** | functional example of how to find the process that called the process that called me |
| **[example_of_getting_real_path_of_executable](https://gitlab.com/mubunt/example_of_getting_real_path_of_executable)** | functional example of how to get the real path name of the current executable |
| **[example_of_loading_modules_on_demand](https://gitlab.com/mubunt/example_of_loading_modules_on_demand)** | functional example of loading modules on demand |
| **[example_of_using_winch_signal](https://gitlab.com/mubunt/example_of_using_winch_signal)** | functional example of the use of the ***winch*** signal emitted during a change in size of the terminal / window |


# LIST OF DEVELOPMENTS REGISTERED HERE


| Project | Description | Language | Screenshot |
| :------ | :---------- | :------- | :-------- |
| **[anais](https://gitlab.com/mubunt/anais)** | a Linux Markdown table textual generator. | C | [here](https://gitlab.com/mubunt/anais/-/blob/master/README_images/anais-01.png)
| **[ansiShadow](https://gitlab.com/mubunt/ansiShadow)** | a Text to ANSI Block Character Generator. | C | [here](https://gitlab.com/mubunt/ansiShadow/-/blob/master/README_images/ansiShadow-01.png)
| **[appSetup](https://gitlab.com/mubunt/appSetup)** | **[OUTDATED]** a Linux graphic utility to generate self-extractable archives. | JAVA | |
| **[aptlog](https://gitlab.com/mubunt/aptlog)** | friendly display of **apt** logs (Ubuntu-like system update). | C | [here](https://gitlab.com/mubunt/aptlog/-/blob/master/README_images/aptlog-01.png)
| **[balls](https://gitlab.com/mubunt/balls)** | Bouncing balls in a Linux xterm-like window. | C | [here](https://gitlab.com/mubunt/balls/-/blob/master/README_images/balls-01.png)
| **[crossmon](https://gitlab.com/mubunt/crossmon)** | cross monitoring, example with two processes (Linux). | C | |
| **[docSearch](https://gitlab.com/mubunt/docSearch)** | search for a word or group of words in a set of pdf files (Linux). | C | |
| **[emdraw](https://gitlab.com/mubunt/emdraw)** | a Linux application to draw of a Euro-million or Loto grid. | C | |
| **[emma](https://gitlab.com/mubunt/emma)** | a Linux application to teach young children the basics of programming. | C | [here](https://gitlab.com/mubunt/emma/-/blob/master/README_images/emma-04.png)
| **[format](https://gitlab.com/mubunt/format)** | a Linux utility formatting a text read on stdin according to French or English typographic rules. Based on the *libFormat* library. | C | |
| **[frame](https://gitlab.com/mubunt/frame)** | a Linux utility displaying in a frame what is read on "stdin". Based on the *libFrame* library. | C | |
| **[histo](https://gitlab.com/mubunt/histo)** | a Linux graphical application that calculates the appearance frequencies of numbers read in a user file and displays the histogram. in a xterm-like terminal. | C | [here](https://gitlab.com/mubunt/histo/-/blob/master/README_images/histo03.png)
| **[infoVault](https://gitlab.com/mubunt/infoVault)** | a Linux application to manage sensitive information at home. | JAVA | |
| **[infoVaultKey](https://gitlab.com/mubunt/infoVaultKey)** | a Linux application to help to retrieve its own *infoVault* encryption key. | JAVA | |
| **[jemma](https://gitlab.com/mubunt/jemma)** | an application to teach young children the basics of programming. | JAVA | [here](https://gitlab.com/mubunt/jemma/-/blob/master/README_images/jemma-01.png)
| **[kong](https://gitlab.com/mubunt/kong)** | a Linux service console for server-type applications. | C | [here](https://gitlab.com/mubunt/kong/-/blob/master/README_images/kong01.png)
| **[kserver](https://gitlab.com/mubunt/kserver)** | a Linux prototype of a bi-threaded server to be used as test vehicule for *kong* application. | C | [here](https://gitlab.com/mubunt/kserver/-/blob/master/README_images/kong02.png)
| **[libAlloc](https://gitlab.com/mubunt/libAlloc)** | a Linux personalized memory dynamic allocation and release routines with a view to tracing and statistics. | C | |
| **[libFormat](https://gitlab.com/mubunt/libFormat)** | a C library for Linux to format a piece of text according to French or English typographic rules. | C | |
| **[libFrame](https://gitlab.com/mubunt/libFrame)** | a C library or Linux to display, in a frame, a set of lines after buffering them. | C | [here](https://gitlab.com/mubunt/libframe/-/blob/master/README_images/4.png)
| **[libMsg](https://gitlab.com/mubunt/libMsg)** | a C library for Linux to display an error or any other information in a text box in the middle of the current xterm-like window. | C | [here](https://gitlab.com/mubunt/libMsg/-/blob/master/README_images/libMsg02.png)
| **[libPager](https://gitlab.com/mubunt/libPager)** | a C library for Linux to view text files one page at a time. | C | [here](https://gitlab.com/mubunt/libPager/-/blob/master/README_images/2.png)
| **[libProgressBar](https://gitlab.com/mubunt/libProgressBar)** | a C library for Linux to display a xterm-like progress bar. | C | [here](https://gitlab.com/mubunt/libProgressBar/-/blob/master/README_images/3.png)
| **[libSplash](https://gitlab.com/mubunt/libSplash)** | a C library for Linux to display a 'dynamic' splash screen to be used by any C application. | C | [here](https://gitlab.com/mubunt/libSplash/-/blob/master/README_images/libSplash02.png)
| **[libTom](https://gitlab.com/mubunt/libtom)** | a C library for Linux to generate a self-extractable archive. | C | |
| **[libZoe](https://gitlab.com/mubunt/libZoe)** |  a C library for Linux providing an API for developing simple text-based environments. | C | [here](https://gitlab.com/mubunt/libZoe/-/blob/master/README_images/zoe_03.png)
| **[quizFlags](https://gitlab.com/mubunt/quizFlags)** | QUIZ: All Country Flags of the World (Linux game)| C | [here](https://gitlab.com/mubunt/quizFlags/-/blob/master/README_images/quizFlags-01.png)
| **[miscSymb](https://gitlab.com/mubunt/miscSymb)** | a Linux program to display miscellaneous symbols and pictographs on Linux. | C | [here](https://gitlab.com/mubunt/miscSymb/-/blob/master/README_images/1.png)
| **[myCproject](https://gitlab.com/mubunt/myCproject)** | a Linux utility to create my usual tree structure of small C development projects. | C | |
| **[netScan](https://gitlab.com/mubunt/netScan)** | a Linux local network scanner. | JAVA | [here](https://gitlab.com/mubunt/netScan/-/blob/master/README_images/netScan-00.png)
| **[networkScan](https://gitlab.com/mubunt/networkScan)** | another Linux local network scanner. | JAVA | [here](https://gitlab.com/mubunt/networkScan/-/blob/master/README_images/networkScan-C06.png)
| **[newProject](https://gitlab.com/mubunt/newProject)** | **[OUTDATED]** a Linux utility to create tree structure and source files from user's templates. | JAVA | |
| **[regDisp](https://gitlab.com/mubunt/regDisp)** | a Linux generic & configurable tool to display configuration registers. | C | [here](https://gitlab.com/mubunt/regDisp/-/blob/master/README_images/regDisp-01.png)
| **[registerView](https://gitlab.com/mubunt/registerView)** | a Linux configuration register viewer. | JAVA | [here](https://gitlab.com/mubunt/registerView/-/blob/master/README_images/registerView01.png)
| **[tcpServer](https://gitlab.com/mubunt/tcpServer)** | a Linux multithreaded Socket & Database Server. | JAVA | [here](https://gitlab.com/mubunt/tcpServer/-/blob/master/README_images/tcpServer-03.png)
| **[tom](https://gitlab.com/mubunt/tom)** | a Linux command-line utility to generate a self-extractable archive. Based on the *libTom* library. | C & Bash | [here](https://gitlab.com/mubunt/tom/-/blob/master/README_images/tom02.png)
| **[toolsBar](https://gitlab.com/mubunt/toolsBar)** | **[OUTDATED]** a Linux configurable tools bar. | JAVA | |
| **[traceViewer](https://gitlab.com/mubunt/traceViewer)** | a Linux generic and customizable tabular viewer for traces. | JAVA | [here](https://gitlab.com/mubunt/traceViewer/-/blob/master/README_images/traceViewer02.png)
| **[tracy](https://gitlab.com/mubunt/tracy)** | a simple C trace macros file (output to a text file). | C | |
| **[willy](https://gitlab.com/mubunt/willy)** | a Linux Markdown text-formatting program producing output suitable for simple fixed-width terminal windows. | C | [here](https://gitlab.com/mubunt/willy/-/blob/master/README_images/More.png)
| **[willyconf](https://gitlab.com/mubunt/willyconf)** | a *willy* configuration file generator. | C | [here](https://gitlab.com/mubunt/willyconf/-/blob/master/README_images/willyconf04.png)
| **[yaComment](https://gitlab.com/mubunt/yaComment)** | supplement to *yaTree*: *.comment* file generation and update (Linux). | C | |
| **[yaMore](https://gitlab.com/mubunt/yaMore)** | yet another Linux *more* (file perusal filter for xterm-like viewing). Based on the *libPager* library.| C | [here](https://gitlab.com/mubunt/yaMore/-/blob/master/README_images/example02.png)
| **[yaTom](https://gitlab.com/mubunt/yaTom)** | a Linux interactive utility to generate a self-extractable archive. Based on the *libTom* library. | C & Bash | [here](https://gitlab.com/mubunt/yaTom/-/blob/master/README_images/yaTom01.png)
| **[yaTree](https://gitlab.com/mubunt/yaTree)** | yet another *tree* - list contents of directories in a tree-like format (Linux). | C | [here](https://gitlab.com/mubunt/yaTree/-/blob/master/README_images/xubuntu-terminator-1.png)

***
